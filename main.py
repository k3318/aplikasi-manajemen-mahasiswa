# Aplikasi_Manajemen_Mahasiswa
# I.S :
# F.S :

# Class untuk Node
class Node:
    def __init__(self, info):
        self.info = info
        self.next = None

# Class Untuk Linked List
class LinkedList:
    def __init__(self):
        self.awal = None

    def isEmpty(self):
        return self.awal is None

    def SatuNode(self):
        bantu = self.awal
        if bantu.next is None:
            return True
        else:
            return False

    def tampilData(self):
        print("Isi Linked List : ")
        if self.isEmpty():
            print("Data Kosong")
        else:
            self.UrutNimDesc()
            bantu = self.awal
            while bantu is not None:
                print('----------------------------------')
                print('NIM :', bantu.info['nim'])
                print('Nama :', bantu.info['nama'])
                print('Nilai :', bantu.info['nilai'])
                print('Indeks :', bantu.info['Indeks'])
                bantu = bantu.next
            print()
            print('-------------------------------------')

    # Cek Unik NIM
    def cekUnikNIM(self, nim):
        bantu = self.awal
        ketemu = False
        while not ketemu and bantu is not None:
            if bantu.info['nim'] == nim:
                ketemu = True
            else:
                bantu = bantu.next
        return ketemu


    # Penambahan di Depan
    def sisipDepanSingle(self, mahasiswa):
        baru = Node(mahasiswa)
        if (not self.isEmpty()):
            baru.next = self.awal
        self.awal = baru

    def sisipTengahSingle(self, mahasiswa):
        sisipSetelah = int(input("Sisipkan Setelah: "))
        bantu = self.awal
        ketemu = False
        while (not ketemu) and (bantu is not None):
            if (bantu.info == sisipSetelah):
                ketemu = True
            else:
                bantu = bantu.next

        if (ketemu):
            baru = Node(mahasiswa)
            if (bantu.next is None):
                self.sisipBelakangSingle(mahasiswa)
            else:
                baru.next = bantu.next
                bantu.next = baru
        else:
            print("Data", sisipSetelah, "Tidak Ditemukan")

    # Penambahan di Belakang
    def sisipBelakangSingle(self, mahasiswa):
        baru = Node(mahasiswa)
        if (self.isEmpty()):
            self.awal = baru
        else:
            bantu = self.awal
            while (bantu.next is not None):
                bantu = bantu.next
            bantu.next = baru

    def menuUtama(self):
        print("Menu Aplikasi Data Mahasisiswa LinkedList python");
        print("-------------------------------------------")
        print("[1] Tambah Data Mahaiswa")
        print("[2] Tampilkan Data Mahasiswa")
        print("[0] Keluar")
        pilihan = str(input("Masukkan pilihan anda : "))
        if (pilihan == "1"):
            self.menuTambahData()
        elif (pilihan == "2"):
            self.tampilData()
            self.menuUtama()
        elif (pilihan == "0"):
            pass
            # exit()
        else:
            print("Menu Tidak Ditemukan, Tolong Pilih Menu Yang Tersedia")
            # self.menuUtama()

    def inputDataSisipDepan(self):
        print("-------------------------------------------")
        nim = str(input("Masukan NIM: "))
        nama = str(input("Masukan Nama: "))
        nilai = int(input("Masukan Nilai: "))
        indeks = self.indeksNilai(nilai)

        while self.cekUnikNIM(nim) == True:
            print("NIM SUDAH DIPAKAI, SILAHKAN MASUKAN NIM KEMBALI!")
            nim = str(input("Masukan NIM: "))

        mahasiswa = {
            "nim": nim,
            "nama": nama,
            "nilai": nilai,
            "Indeks": indeks
        }

        self.sisipDepanSingle(mahasiswa)
        # self.tampilData()

    def inputDataSisipBelakang(self):
        print("-------------------------------------------")
        nim = str(input("Masukan NIM: "))
        nama = str(input("Masukan Nama: "))
        nilai = int(input("Masukan Nilai: "))
        indeks = self.indeksNilai(nilai)

        while list1.cekUnikNIM(nim) == True:
            print("NIM SUDAH DIPAKAI, SILAHKAN MASUKAN NIM KEMBALI!")
            nim = str(input("Masukan NIM: "))

        mahasiswa = {
            "nim": nim,
            "nama": nama,
            "nilai": nilai,
            "Indeks": indeks
        }

        list1.sisipBelakangSingle(mahasiswa)
        list1.tampilData()

    def inputDataSisipTengah(self):
        print("-------------------------------------------")
        nim = str(input("Masukan NIM: "))
        nama = str(input("Masukan Nama: "))
        nilai = int(input("Masukan Nilai: "))
        if (nilai > 80 and nilai <= 100):
            indeks = "A"
        elif (nilai > 70):
            indeks = "B"
        elif (nilai > 60):
            indeks = "C"
        elif (nilai > 50):
            indeks = "D"
        elif (nilai < 50):
            indeks = "E"

        while list1.cekUnikNIM(nim) == True:
            print("NIM SUDAH DIPAKAI, SILAHKAN MASUKAN NIM KEMBALI!")
            nim = str(input("Masukan NIM: "))

        mahasiswa = {
            "nim": nim,
            "nama": nama,
            "nilai": nilai,
            "Indeks": indeks
        }

        list1.sisipTengahSingle(mahasiswa)
        list1.tampilData()

    def menuTambahData(self):
        print("-------------------------------------------")
        print("[1] Penambahan Data Mahasiswa di Depan")
        print("[2] Penambahan Data Mahasiswa di Tengah")
        print("[3] Penambahan Data Mahasiswa di Belakang")
        print("[0] Kembali Ke Menu Utama")
        # print("[9] Kembali Ke Menu Utama")
        # print("[0] Keluar")
        pilihan = str(input("Masukkan pilihan anda : "))
        if (pilihan == "1"):
            self.inputDataSisipDepan()
            # self.menuUtama()
        elif (pilihan =="2"):
            self.inputDataSisipTengah()
            # self.menuUtama()
        elif (pilihan == "3"):
            self.inputDataSisipBelakang()
            # self.menuUtama()
        elif (pilihan == "9"):
            pass
            # self.menuUtama()
        elif (pilihan =="0"):
            pass
            # exit()
        else:
            print("Menu Tidak Ditemukan, Tolong Pilih Menu Yang Tersedia")
            self.menuTambahData()

    #Algoritma Pencarian
    def menuPilihanCari(self):
        print('1. NIM')
        print('2. Nama')
        print('3. Indeks')
        print('Masukkan pilihan pencarian:', end='')
        Pilihan = int(input(''))
        while Pilihan < 0 or Pilihan > 3:
            print('Masukkan pilihan 1,2 dan         3 saja!')
            print('Masukkan pilihan pencarian:', end='')
            Pilihan = int(input(''))
        #Proses pemilihan
        if Pilihan == 1:
            self.cariNIM()
        elif Pilihan == 2:
            self.cariNama()
        else:
            self.cariIndeks()

    def cariNama(self):
        if self.isEmpty():
            print('Data Masih Kosong, Harap tambahkan data terlebih dahulu!')
        else:
            print("Masukkan Nama Mahasiswa yang dicari: ", end='')
            nama = input()
            bantu = self.awal
            i = 0
            while bantu is not None:
                if bantu.info['nama'] == nama:
                    i = i + 1
                    print('-----------------')
                    print(i, '.NIM :', bantu.info['nim'])
                    print('Nama :', bantu.info['nama'])
                    print('Nilai :', bantu.info['nilai'])
                    print('Indeks :', bantu.info['Indeks'])
                bantu = bantu.next
            if i == 0:
                print('Mahasiswa dengan Nama:', nama, 'tidak ditemukan!')

    def indeksNilai(self, Nilai):
        indeks = 'E'
        if Nilai >= 80 and Nilai <= 100:
            indeks = 'A'
        elif Nilai >= 70 and Nilai < 80:
            indeks = 'B'
        elif Nilai >= 60 and Nilai < 70:
            indeks = 'C'
        elif Nilai >= 50 and Nilai < 60:
            indeks = 'D'
        return indeks

    def cariNIM(self):
        if self.isEmpty():
            print('Data Masih Kosong, Harap tambahkan data terlebih dahulu!')
        else:
            print("Masukkan NIM yang akan dicari:", end='')
            nim = input()
            bantu = self.awal
            ketemu = False

            while not ketemu and bantu is not None:
                if bantu.info['nim'] == nim:
                    ketemu = True
                else:
                    bantu = bantu.next

            if ketemu:
                print('NIM :', bantu.info['nim'])
                print('Nama :', bantu.info['nama'])
                print('Nilai :', bantu.info['nilai'])
                print('Indeks :', bantu.info['Indeks'])
            else:
                print('NIM :', nim, 'tidak ditemukan!')

    def cariIndeks(self):
        if self.isEmpty():
            print('Data Masih Kosong, Harap tambahkan data terlebih dahulu!')
        else:
            print('Masukkan Indeks dicari:', end='')
            indeks = input()
            bantu = self.awal
            i = 0
            while bantu is not None:
                if bantu.info['Indeks'] == indeks:
                    i = i + 1
                    print('-----------------')
                    print(i, '.NIM :', bantu.info['nim'])
                    print('Nama :', bantu.info['nama'])
                    print('Nilai :', bantu.info['nilai'])
                    print('Indeks :', bantu.info['Indeks'])
                bantu = bantu.next
            if i == 0:
                print('mahasiswa berindeks', indeks, 'tidak ditemukan!')

    def UbahData(self):
        if(self.isEmpty()):
            print('Data Masih Kosong!')
        else:
            #Menampilkan Menu Ubah data
            print('Masukkan NIM yang ingin diubah:', end='')
            NIMubah = input()
            bantu = self.awal
            ketemu = False

            while not ketemu and bantu is not None:
                if bantu.info['nim'] == NIMubah:
                    ketemu = True
                else:
                    bantu = bantu.next
            if ketemu:
                print('1. NIM')
                print('2. Nama')
                print('3. Nilai')
                print('Masukkan elemen yang ingin diubah:', end='')
                pilihan = int(input())
                while pilihan < 0 or pilihan > 3:
                    print('Pilihan tidak ada dalam Menu, harap masukkan kembali pilihan menu!')
                    pilihan = int(input())
                if pilihan == 1:
                    print('Masukkan NIM baru:', end='')
                    NIMBaru = input('')
                    while self.cekUnikNIM(NIMBaru):
                        print('NIM Tersebut telah tersedia!')
                        print('Masukkan NIM Baru:', end='')
                        NIMBaru = input()
                    bantu.info['nim'] = NIMBaru
                elif pilihan == 2:
                    print('Masukkan Nama Baru:', end='')
                    NamaBaru = input()
                    bantu.info['nama'] = NamaBaru
                else:
                    print('Masukkan Nilai Baru:', end='')
                    NilaiBaru = float(input())
                    bantu.info['nilai'] = NilaiBaru
                    bantu.info['Indeks'] = self.indeksNilai(NilaiBaru)
            else:
                print('NIM :',NIMubah,'Tidak ditemukan!')
    def UrutNimDesc(self):
        i = self.awal
        while i.next is not None:
            max = i
            j = i.next
            while j is not None:
                if j.info['nim'] > max.info['nim']:
                    max = j
                j = j.next
            #Proses pertukaran data
            temp = max.info
            max.info = i.info
            i.info = temp
            #menempatkan pointer i ke simpul berikutnya
            i = i.next

    #Menu Penghapusan
    def MenuHapusData(self):
            print("|==============================|")
            print("1. Penghapusan Data Mahasiswa di Depan")
            print("2. Penghapusan Data Mahasiswa di Belakang")
            print("3. Penghapusan Data Mahasiswa di Tengah")
            print("0. Kembali Ke Menu Utama")
            pilihan = int(input("Masukkan pilihan anda : "))
            while pilihan < 0 or pilihan > 3:
                print("Pilihan Tidak Tersedia")
                pilihan = int(input("Masukkan pilihan anda : "))
            if (pilihan == 1):
                self.HapusDepanSingle()
            elif (pilihan == 2):
                self.HapusBelakangSingle()
            elif (pilihan == 3):
                self.HapusTengahSingle()
            elif (pilihan == 0):
                pass
            else:
                print("Menu Tidak Tersedia, ")
                self.MenuHapusData()

    # Penghapusan Depan
    def HapusDepanSingle(self):
        if self.isEmpty():
            print('Data Kosong')
        else:
            phapus = self.awal
            elemen = phapus.info
            if(self.SatuNode()):
                self.awal = None
            else:
                self.awal = phapus.next

            del phapus
            print('Data yang Dihapus Adalah:', elemen)

    # Penghapusan Belakang
    def HapusBelakangSingle(self):
        if self.isEmpty():
            print('Data Kosong')
        else:
            phapus = self.awal
            if self.SatuNode():
                self.awal = None
            else:
                while phapus.next is not None:
                    phapus = phapus.next

                bantu = self.awal
                while bantu.next is not phapus:
                    bantu = bantu.next

                bantu.next = None
                elemen = phapus.info
                del phapus
                print('Data yang dihapus Adalah: ', elemen)

    # Penghapusan Di Tengah
    def HapusTengahSingle(self):
        if self.isEmpty():
            print('Data Kosong')
        else:
            DataHapus = str(input('Masukkan NIM yang akan Dihapus : '))
            phapus = self.awal
            ketemu = False
            while not ketemu and phapus is not None:
                if phapus.info['nim'] == DataHapus:
                    ketemu = True
                else:
                    phapus = phapus.next

            if ketemu:
                elemen = phapus.info['nim']
                if phapus == self.awal:
                    self.HapusDepanSingle()
                elif phapus.next is None:
                    self.HapusBelakangSingle()
                else:
                    bantu = self.awal
                    while bantu.next is not phapus:
                        bantu = bantu.next

                    bantu.next = phapus.next
                    del phapus
                    print("Data yang dihapus adalah:", elemen)
            else:
                print('NIM', DataHapus, 'Tidak Ditemukan')

def menuUtama(list1):
    # Print Main Menu
    print("|============ MENU ============|")
    print("1. Penambahan Data Mahasiswa")
    print("2. Penghapusan Data Mahasiswa")
    print("3. Pencarian Data Mahasiswa")
    print("4. Pengubahan Data Mahasiswa")
    print("5. Tampil Data Mahasiswa")
    print("0. EXIT")
    print("|==============================|")

    # Pemilihan Menu Oleh User
    pilih = int(input("Pilih Angka (1-5): "))
    while pilih < 0 or pilih > 5:
        print("Pilihan Tidak Tersedia")
        pilih = int(input("Pilih Angka (1-5): "))
    # Menampilan Menu Yang Dipilih User
    if(pilih==1):
        list1.menuTambahData()

    elif(pilih==2):
        print("|==============================|")
        print("Penghapusan Data Mahasiswa")
        list1.MenuHapusData()

    elif(pilih==3):
        print("Pencarian Data Mahasiswa")
        list1.menuPilihanCari()

    elif(pilih==4):
        print("Pengubahan Data Mahasiswa")
        list1.UbahData()

    elif(pilih==5):
        print("Tampil Data Mahasiswa")
        list1.tampilData()
    else:
        #Penghancuran semua data
        return
    menuUtama(list1)

list1 = LinkedList()
menuUtama(list1)
